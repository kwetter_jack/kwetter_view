@echo off
:: variables
set IMAGE=jmutsers/kwetter-view
set CONTAINER=kwetter-view

git describe --always --abbrev --tags --long > result.txt
set /p GIT_VERSION=<result.txt

set PORTS=9091:9091
set DOCKER_USERNAME=jmutsers

:: Build and set tag for image
docker build -t "%IMAGE%:%GIT_VERSION%" .
docker tag %IMAGE%:%GIT_VERSION% %IMAGE%:latest

:: Log in to Docker Hub and push
cat docker_password.txt | docker login -u %DOCKER_USERNAME% --password-stdin
docker push %IMAGE%:%GIT_VERSION%
docker push %IMAGE%:latest

:: Stop and remove old version
docker ps -q -f name=%CONTAINER% > result.txt
for /f %%i in ("result.txt") do set size=%%~zi
if %size% gtr 0 (
    docker stop %CONTAINER%
    docker rm %CONTAINER%
)


:: get all old images
docker ps --format '{{.Image}}' | grep '%IMAGE%' > containers.txt
docker images --format '{{.Repository}}:{{.Tag}}' | grep '%IMAGE%' > images.txt
grep -v -F -f containers.txt images.txt > result.txt
del containers.txt
del images.txt

:: remove all old images
cat result.txt | xargs docker rmi
docker images --filter "dangling=true" | xargs docker rmi

del result.txt
