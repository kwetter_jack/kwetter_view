#!/bin/bash

IMAGE=$1
VERSION=$2
FILE=dev/${IMAGE}-deployment.yaml

chmod 777 ${FILE}
sed -i "s/${IMAGE}/${IMAGE}:${VERSION}/g" ${FILE}
cat ${FILE}
