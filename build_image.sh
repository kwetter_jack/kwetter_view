#!/bin/sh
set -e # Stop script from running if there are any errors

CONTAINER="$1"                                 # Docker container name
GIT_VERSION="$3" # Git hash and tags
DOCKER_USERNAME="jmutsers"
DOCKER_PASSWORD="$2"
IMAGE=${DOCKER_USERNAME}/${CONTAINER}

echo ${IMAGE}
echo "${IMAGE}:${GIT_VERSION}"

# Build and tag image
docker build -t ${IMAGE}:${GIT_VERSION} .
docker tag ${IMAGE}:${GIT_VERSION} ${IMAGE}:latest

# Log in to Docker Hub and push
echo "${DOCKER_PASSWORD}" | docker login -u "${DOCKER_USERNAME}" --password-stdin
docker push ${IMAGE}:${GIT_VERSION}
docker push ${IMAGE}:latest