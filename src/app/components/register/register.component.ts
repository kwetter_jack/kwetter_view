import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService, AuthenticationService } from '../../_services';
import { Registration } from '../../_models';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  formRegistration: Registration = new Registration();
  confirmPassword: string = "";
  passwordsMatch: boolean = true;
  loading = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private service: AuthenticationService,
    private alertService: AlertService) { 
      this.registerForm = this.formBuilder.group({
        displayName: ['', Validators.required],
        email: ['', Validators.required],
        password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  ngOnInit() {
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onSubmit() {
      this.submitted = true;

      // stop here if form is invalid
      if (this.registerForm.invalid || !this.passwordsMatch) {
          return;
      }

      this.loading = true;
      this.service.registration(this.formRegistration)
        .pipe(first())
        .subscribe(
          data => {
            this.router.navigate(["/"]);
          },
          error => {
            console.log(error);
            this.alertService.error(error);
            this.loading = false;
          }
        );
  }


  onChangeConfirmPassword(){
    this.passwordsMatch = this.confirmPassword == this.formRegistration.password;
  }

}
