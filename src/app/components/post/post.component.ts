import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Comment, Post } from 'app/_models';
import { CommentService } from 'app/_services/comment.service';
import { PostService } from 'app/_services/post.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  post_id: string;
  commentForm: FormGroup;
  post:Post = new Post();
  comments: Comment[] = [];
  newcomment: Comment = new Comment();
  loading = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private postService: PostService,
    private commentService: CommentService
  ) {
    this.post_id = this.route.snapshot.paramMap.get('id') || "";
    this.commentForm = this.formBuilder.group({
      message: ['', Validators.required]
    });

  }

  ngOnInit(): void {
    this.loadPost();
  }

  private loadPost(){
    this.postService.getPost(this.post_id).subscribe((data)=>{
      this.post = data;
      this.comments = this.post.comments;
      console.log(this.post);
      console.log("post loaded");
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.commentForm.controls; }

  onSubmit(){
    this.submitted = true;

    // stop here if form is invalid
    if (this.commentForm.invalid) {
      return;
    }

    this.loading = true;
    
    console.log("create new comment");
    console.log(this.post);

    this.newcomment.post_id = this.post.id;

    this.commentService.create(this.newcomment)
      .subscribe(
        data => {
          console.log("added new comment");
          let currentUrl = this.router.url;
          this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
              this.router.navigate([currentUrl]);
          });
        },
        error => {
          console.log(error);
          this.loading = false;
        }
      );
  }

}
