import { Component, OnInit, inject, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common'; 
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { faEllipsis } from '@fortawesome/free-solid-svg-icons';

import { PostService } from 'app/_services/post.service';
import { Post } from 'app/_models';
import { Router } from '@angular/router';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  postForm: FormGroup;
  post:Post = new Post();
  
  postUpdateForm: FormGroup;
  postUpdate: Post = new Post();

  posts: Post[] = [];
  loading = false;
  submitted = false;
  faEllipsis = faEllipsis;
  doc: Document;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private postService: PostService,
    @Inject(DOCUMENT) document: Document
  ) {
    this.doc = document
    this.postForm = this.formBuilder.group({
      message: ['', Validators.required]
    });

    this.postUpdateForm = this.formBuilder.group({
      message: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.loadMyPosts();
  }

  private loadMyPosts(){
    var accountData = JSON.parse(localStorage.getItem('currentUser') || "{}");

    this.postService.getAll(accountData.localId).subscribe((data)=>{
      this.posts = <Post[]> data;
    });
  }

  public fixDate(date: string){
    date = date.replace("T", " ");
    return date;
  }

  public toggleMenu(element: string){
    let el = this.doc.getElementById(element);
    let cover = this.doc.getElementById("screen-cover");
    if(!el || !cover){
      return;
    }
    
    if(el.classList.contains("active")){
      cover.classList.remove("active")
      el.classList.remove("active");
    }else{
      cover.classList.add("active")
      el.classList.add('active');
    }
  }

  public closeMenu(){
    const elements: Element[] = Array.from(this.doc.getElementsByClassName("post_menu_box active"));
    
    elements.forEach(element => {
      element.classList.remove("active");
    });

    let el = this.doc.getElementById("screen-cover");
    el?.classList.remove("active")
  }

  public editPost(post:Post){
    console.log(post);
    this.postUpdate = post;
    
    let post_body_id = "post_body" + post.id;
    let post_body = this.doc.getElementById(post_body_id);
    
    let form_id = "bodyEditor" + post.id;
    let form = this.doc.getElementById(form_id);

    post_body?.classList.add("hidden");
    form?.classList.add('active');

    this.closeMenu();
  }

  public deletePost(post:Post){
    this.postService.delete(post.id).subscribe();

    this.posts.forEach((element, index) =>{
      if(element.id == post.id){
        this.posts.splice(index, 1);
      }
    });

    this.closeMenu();
  }

  // convenience getter for easy access to form fields
  get f() { return this.postForm.controls; }
  get g() { return this.postUpdateForm.controls; }

  onSubmitUpdate(){
    this.submitted = true;

    // stop here if form is invalid
    if (this.postUpdateForm.invalid) {
      return;
    }

    this.loading = true;
    
    this.postService.update(this.postUpdate)
      .subscribe(
        data => {
          let currentUrl = this.router.url;
          this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
              this.router.navigate([currentUrl]);
          });
        },
        error => {
          console.log(error);
          this.loading = false;
        }
      );
  }

  onSubmit(){
    this.submitted = true;

    // stop here if form is invalid
    if (this.postForm.invalid) {
      return;
    }

    this.loading = true;
    this.post.loadFromForm(this.post);
    
    this.postService.create(this.post)
      .subscribe(
        data => {
          let currentUrl = this.router.url;
          this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
              this.router.navigate([currentUrl]);
          });
        },
        error => {
          console.log(error);
          this.loading = false;
        }
      );
  }


}
