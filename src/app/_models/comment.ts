export interface IComment{
    id: number;
    writer: string;
    post_id: number;
    comment_id: number;
    message: string;
    creation_date: string | null;
}

export class Comment implements IComment{
    constructor(public id = 0, public writer = "", public message = "", public post_id = 0, public comment_id = 0, public creation_date = null){}

    // load with html form data
    public loadFromForm(object: IComment){ 
        this.message = object.message;
        this.post_id = object.post_id;
    }
}