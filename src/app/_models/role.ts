export enum Role {
    User = 'USER',
    Owner = 'OWNER',
    Employee = 'EMPLOYEE'
}