export interface IPasswordEditor{
    password: string;
    confirmPass: string;
}

export class PasswordEditor implements IPasswordEditor{
    password = "";
    confirmPass = "";
}