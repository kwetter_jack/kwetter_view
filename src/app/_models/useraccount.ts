export interface IUseraccount{
    localId: string;
    email: string;
    displayName: string;
    role: string;
    idToken?: string;
}

export class UserAccount implements IUseraccount{
    localId = "";
    displayName = ""
    email = "";
    role = "";
    idToken? = "";
 
    // load from api response
    public loadFromObject(object: IUseraccount){
        this.localId = object.localId;
        this.displayName = object.displayName;
        this.email = object.email;
        this.role = object.role;
        this.idToken = object.idToken;
    }

    // load with html form data
    public loadFromForm(object: IUseraccount){ 
        this.email = object.email;
        this.displayName = object.displayName;
    }
}