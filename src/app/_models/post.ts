import { Comment } from './comment';

export interface IPost{
    id: number;
    group_id: number;
    message: string;
    creation_date: string | null;
    writer: string;
    comments: Comment[];
}

export class Post implements IPost{
    constructor(public id = 0, public group_id = 0, public message = "", public creation_date = "", public writer = "", public comments = []){}
    
    // load with html form data
    public loadFromForm(object: IPost){ 
        this.message = object.message;
        this.group_id = object.group_id || 0;
    }
}