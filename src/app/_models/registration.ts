export interface IRegistration{
    email: string;
    displayName: string;
    password: string;
}

export class Registration implements IRegistration{
    displayName = ""
    email = "";
    password = "";
 
    // load with html form data
    public loadFromForm(object: IRegistration){ 
        this.email = object.email;
        this.password = object.password;
        this.displayName = object.displayName;
    }
}