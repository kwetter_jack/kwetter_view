import { Component, HostListener, OnInit } from '@angular/core';
import { AuthenticationService } from '../../_services/authentication.service';
import { faUserCircle } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-user-layout',
  templateUrl: './user-layout.component.html',
  styleUrls: ['./user-layout.component.css']
})

export class UserLayoutComponent implements OnInit {
  displayName: string;
  userIcon = faUserCircle;
  
  constructor(private authService: AuthenticationService) { 
    this.displayName = "";
  }
  
  ngOnInit(): void {
    var currentUser = JSON.parse(localStorage.getItem('currentUser') || '{}');
    this.displayName = currentUser?.displayName;
    console.log('displayName', this.displayName);
  }

  onLogout() {
    this.authService.logout();
  }

  /* Set the width of the side navigation to 250px */
  openNav() {
    var sidebar = document.getElementById("mySidenav")
    if(sidebar){
      sidebar.style.width = "300px";
    }
  }

  /* Set the width of the side navigation to 0 */
  closeNav() {
    var sidebar = document.getElementById("mySidenav")
    if(sidebar){
      sidebar.style.width = "0";
    }
  }

  @HostListener('window:mouseup', ['$event'])
  mouseUp(event: { target: HTMLElement | null; }){
    var sideNav = document.getElementById("mySidenav");
    if(event.target != sideNav){
      this.closeNav();
    }
  }

}