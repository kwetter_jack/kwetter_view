import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from './../../environments/environment';

import { Registration, UserAccount } from '../_models';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<UserAccount>;
    private currentUserToken: BehaviorSubject<string>;
    public currentUser: Observable<UserAccount>;

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<UserAccount>(JSON.parse(localStorage.getItem('currentUser') || "{}"));
        this.currentUserToken = new BehaviorSubject<string>(JSON.parse(localStorage.getItem('userToken') || "{}"));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): UserAccount {
        return this.currentUserSubject.value;
    }

    public get currentUserTokenValue(): string {
        return this.currentUserToken.value;
    }

    login(email: string, password: string) {

        var header = {
            headers: new HttpHeaders()
        }

        var requestBody = {
            email: email,
            password: password,
            returnSecureToken: true
        }

        console.log(environment.apiUrl);
        return this.http.post<any>(`http://` + environment.apiUrl + `/auth/login`, requestBody, header)
            .pipe(map(response => {
                console.log(response);
                if (response && response.idToken){
                    this.currentUserToken.next(response.idToken);

                    // store user details and basic auth credentials in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(response));
                    localStorage.setItem('userToken', JSON.stringify(response.idToken));
                    this.currentUserSubject.next(response);
                    console.log("response");
                    return response;
                }
            }));
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        localStorage.removeItem('userToken');
        this.currentUserSubject.next(new UserAccount());
        this.currentUserToken.next("");
    }

    registration(registerForm: Registration){
        console.log(`http://` + environment.apiUrl + `/user/register`);
        return this.http.post<any>(`http://` + environment.apiUrl + `/user/register`, registerForm);
    }
}