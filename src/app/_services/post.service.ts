import { Injectable, ɵresetJitOptions } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParamsOptions } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from './../../environments/environment';

import { Post} from '../_models';

@Injectable({ providedIn: 'root' })
export class PostService {
    constructor(private http: HttpClient) { }

    getAll(id: number){
        return this.http.get<any>(`http://` + environment.apiUrl + `/post/user_posts/${id}`)
        .pipe(map(response => {
            return response;
        }));
    }

    getPost(id: string){
        return this.http.get<any>(`http://` + environment.apiUrl + `/post/get/${id}`)
        .pipe(map(response => {
            return response;
        }));
    }

    create(post: Post){
        return this.http.post<any>(`http://` + environment.apiUrl + `/post/create`, post)
        .pipe(map(response => {
            return response;
        }));
    }

    update(post: Post){
        return this.http.put<any>(`http://` + environment.apiUrl + `/post/update`, post);
    }

    delete(id: number){
        return this.http.delete(`http://` + environment.apiUrl + `/post/delete/${id}`,  {responseType: 'text'});
    }


}