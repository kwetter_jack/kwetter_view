import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Comment } from 'app/_models';

@Injectable({ providedIn: 'root' })
export class CommentService {
    constructor(private http: HttpClient) { }

    create(comment: Comment){
        return this.http.post<any>(`http://` + environment.apiUrl + `/comment/create`, comment);
    }

    update(comment: Comment){
        return this.http.put<any>(`http://` + environment.apiUrl + `/comment/update`, comment);
    }

    delete(id: number){
        return this.http.delete<any>(`http://` + environment.apiUrl + `/comment/delete/${id}`);
    }
}