(function(window) {
    window["env"] = window["env"] || {};
  
    // Environment variables
    window["env"]["apiUrl"] = "localhost:4000";
    window["env"]["debug"] = true;
  })(this);